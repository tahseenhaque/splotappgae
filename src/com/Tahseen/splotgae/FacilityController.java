package com.Tahseen.splotgae;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FacilityController {

	String message;	

	@Autowired
	Facility downtownFacility;

	@RequestMapping(value="/test/{name}", method = RequestMethod.GET)
	public String getMovie(@PathVariable String name, ModelMap model) {

		model.addAttribute("movie", name);
		model.addAttribute("message", this.message);

		//return to jsp page, configured in mvc-dispatcher-servlet.xml, view resolver
		return "test";

	}

	public void setMessage(String message) {
		this.message = message;
	}

	@RequestMapping(value="/", method=RequestMethod.GET)
	public String frontPage(ModelMap model)
	{
		return "index";		
	}

	@RequestMapping(value="/index", method=RequestMethod.GET)
	public String welcomePage(ModelMap model)
	{
		return "redirect:/";		
	}

	@RequestMapping(value="/booking", method=RequestMethod.GET)
	public String blueBooking(ModelMap model) throws Exception
	{
		for(ISpace lot:downtownFacility.getAllLots()){
			if(lot.getDriver() == null)	
				lot.ReleaseSpot();
		}  

		String modifiedSentence = "";

		try {
			modifiedSentence = ""; 

			List<String> list = new ArrayList<String>();
			for(int i = 0; i < 12; i++){
				list.add(i, "false");
			}

			List<String> list_fromServer = Arrays.asList(modifiedSentence.split("\\s"));

			int foo;
			for (int i = 0; i < list_fromServer.size() && list_fromServer.size() != 0; i++) {
				foo = Integer.parseInt(list_fromServer.get(i));
				list.set(foo, "true");
				downtownFacility.getAllLots().get(foo).ReserveSpot(null);				
			}

		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Custom Message: Server not alive!");
		}		

//		int i = 0;
//		boolean[] lotReserved = new boolean[12];
		for(ISpace lot:downtownFacility.getAllLots()){
			if(lot.getIsReserved()) {	
			//	lotReserved[i] = true;
				model.addAttribute("lot" + String.valueOf(lot.getLotNumber()), 1);
			}
			else {
				model.addAttribute("lot" + String.valueOf(lot.getLotNumber()), 0);
			}
		//	i++;
		}

		//need json to use arraylist in jsp
		model.addAttribute("lots", downtownFacility.getAllLots());
		
		Customer customer = new Customer();
		model.addAttribute(customer);

		return "booking";		
	}

	@RequestMapping(value="/booking", method = RequestMethod.POST)
	public String blueReserveLot(@ModelAttribute("customer") Customer customer, BindingResult result) throws Exception
	{
		if (result.hasErrors()) 
			return "booking";

		downtownFacility.reserveLot(customer.getLotNumber(), customer);

		return "services";
	}

	@RequestMapping(value="/release", method=RequestMethod.GET)
	public String releaseLots(ModelMap model)
	{
		for(ISpace lot:downtownFacility.getAllLots()){
			lot.ReleaseSpot();
		} 

		return "index";		
	}

	@RequestMapping(value="/contact", method=RequestMethod.GET)
	public String blueContact(ModelMap model)
	{
		return "contact";		
	}	
}
